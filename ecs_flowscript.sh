#!/bin/bash
# Job name:
#SBATCH --job-name=stokes_2E_twicerefined
#
# Project:
#SBATCH --account=nn9279k
# Wall clock limit:
#SBATCH --time='120:00:00'
#
#
# Max memory usage per task:
#SBATCH --mem-per-cpu=3900M
#
# Number of tasks (cores):
#SBATCH --ntasks=896 --nodes=56
#SBATCH --hint=compute_bound
#SBATCH --cpus-per-task=1

## Set up job environment
source /cluster/bin/jobsetup

echo $SCRATCH

source ~johannr/fenics-dev-2016.04.06.abel.gnu.conf

# Expand pythonpath with locally installed packages
# export PYTHONPATH=$PYTHONPATH:$HOME/.local/lib/python2.7/site-packages/

# Define what to do when job is finished (or crashes)
cleanup "mkdir -p /work/users/karleh/ecs_flow"
cleanup "mkdir -p /work/users/karleh/ecs_flow/$SLURM_JOB_ID"
cleanup "cp -r $SCRATCH/src/output_files /work/users/karleh/ecs_flow/$SLURM_JOB_ID"

# Copy necessary files to $SCRATCH
cp -r /usit/abel/u1/karleh/extracellular_flow/src $SCRATCH

cp /usit/abel/u1/karleh/data/kinney_meshes/2E_cropped_refined_twice.xdmf $SCRATCH/src/
cp /usit/abel/u1/karleh/data/kinney_meshes/2E_cropped_refined_twice.h5 $SCRATCH/src/





# Enter $SCRATCH and run job
cd $SCRATCH/src

mpirun -np 896 python kinney_stokes.py 2E_cropped_refined_twice.xdmf minres hypre_amg 50 7500 P1P1 x
