from dolfin import (TrialFunctions, Function, VectorElement, FiniteElement,
                    MixedElement, FunctionSpace, TestFunctions, inner, grad,
                    div, dot, Constant, FacetNormal, dx, CellSize,
                    DirichletBC, assemble, assemble_system, parameters,
                    PETScKrylovSolver, sqrt, interpolate
                    )
from base_problem import BaseProblem
from utils import print_message

class StokesProblem(BaseProblem):

    def __init__(self, argdict, domain,
                 u_D_subdomains, p_N_subdomains, symm_subdomains):
        self.mu = argdict["mu"]

        self.atol = argdict["atol"]
        self.rtol = argdict["rtol"]
        self.gmres_restarts = argdict["gmres_restarts"]
        self.max_its = argdict["max_its"]
        
        self.initial_guess = argdict["initial_guess"]
        self.suppress_debug = argdict["suppress_debug"]
        self.debug_mode = argdict["debug_mode"]
        self.solver = argdict["solver"]
        self.preconditioner = argdict["preconditioner"]
        
        self.domain = domain
        self.beta = argdict["beta"]

        self.use_P1P1 = argdict["use_P1P1"]
        self.W = self.function_space()
        self._matrices = None

        from dolfin import Constant
        self.u_dirichlet_subdomains = u_D_subdomains
        self.p_neumann_subdomains = p_N_subdomains
        self.symmetry_subdomains = symm_subdomains

    @staticmethod        
    def parameters():
        """Expected arguments and descriptions."""
        args = {
            "mu": ( "Viscosity of fluid.", 1E-3),
            "atol": ("Absolute tolerance.", 1E-9),
            "rtol": ("Relative tolerance.", 1E-9),
            "initial_guess": (
                "Use given expression as initial iterative solver guess.", None),
            "suppress_debug": ("Suppress convergence output.", False),
            "debug_mode": ("Flag for debugging purposes.", False),
            "preconditioner": ("Preconditioner.", "hypre_amg"),
            "solver": ("Solver.", "gmres"),
            "gmres_restarts": ( # PETSc's default is 30
                "Number of restarts (search directions) for gmres solver.",50),
            "beta": ("Stabilization parameter.", 0.2),
            "max_its": ("Maximum number of iterations.", 10000),
            "use_P1P1": ("Use P1-P1 elements and stabilisation.", True),
            
        }
        return args


    def function_space(self):

        elt = self.domain.mesh.ufl_cell()

        if self.use_P1P1:
            print_message("Creating P1-P1 FunctionSpace.")
            P1_u = VectorElement("Lagrange", elt, 1)
            P1_p = FiniteElement("Lagrange", elt, 1)
            P1P1 = MixedElement([P1_u, P1_p])
            return FunctionSpace(self.domain.mesh, P1P1)
        else:
            print_message("Creating Taylor-Hood FunctionSpace.")
            P2 = VectorElement("Lagrange", elt, 2)
            P1 = FiniteElement("Lagrange", elt, 1)
            TH = MixedElement([P2, P1])
            return FunctionSpace(self.domain.mesh, TH)

    def u_dirichlet_bcs(self, bcs={}):
        # dirichlet_subdomains should hold pairs (n: val)
        # where val is the value of the solution at subdomain n
        # as a FEniCS expression

        return [DirichletBC(self.W.sub(0), bcs[subdomain_id],
                            self.domain.subdomain_data, subdomain_id)
                for subdomain_id in bcs]

    def symmetry_bcs(self, subdomains={}):
        # returns a list of Dirichlet symmetry bcs on the specified subdomains
        # subdomains should be a dict (i: dim) where i is the
        # subdomain id and dim is the coordinate vector perpendicular to
        # subdomain i (i.e. 0 if subdomain i is perpendicular to the x-axis
        # , 1 if y, 2 if z)

        return [DirichletBC(self.W.sub(0).sub(subdomains[subdomain_id]),
                            Constant(0), self.domain.subdomain_data,
                            subdomain_id)
                for subdomain_id in subdomains]

    def p_neumann_terms(self, ds, dot_v_n, bcs):
        # bcs should hold pairs (n: val) where val is the value of
        # the pressure term at subdomain n as a FEniCS expression
        
        return sum([bcs[subdomain_id] * dot_v_n * ds(subdomain_id)
                    for subdomain_id in bcs])

    
    def modify_neumann_bcs(self, new_neumann):
        for subdomain_id in new_neumann:
            self.p_neumann_subdomains[subdomain_id] = new_neumann[subdomain_id]

    
    def solve(self):
        ds = self.domain.measure("ds")

        print_message("Creating Test/TrialFunctions.")
        u, p = TrialFunctions(self.W)
        v, q = TestFunctions(self.W)
        w = Function(self.W)

        n = FacetNormal(self.domain.mesh)
        beta = self.beta
        h = CellSize(self.domain.mesh)

        mu = self.mu
        if self.use_P1P1:
            print_message("Using stabilized variational form with beta = {}".format(beta))
            a = (mu*inner(grad(u), grad(v))*dx + div(v)*p*dx + div(u)*q*dx
                 - beta*h*h*inner(grad(p), grad(q))*dx)
            b = (mu*inner(grad(u), grad(v))*dx + p*q/mu*dx
                 + beta*h*h*inner(grad(p), grad(q))*dx)
        else:
            print_message("Using unstabilized variational form")
            a = mu*inner(grad(u), grad(v))*dx + div(v)*p*dx + div(u)*q*dx
            b = mu*inner(grad(u), grad(v))*dx + p*q/mu*dx


        print_message("Creating Dirichlet BCs for velocity.")
        dirichlet_bcs = self.u_dirichlet_bcs(self.u_dirichlet_subdomains)

        if len(self.symmetry_subdomains) > 0:
            print_message("Creating symmetric BCs for velocity.")
            symmetry_bcs = self.symmetry_bcs(self.symmetry_subdomains)
            dirichlet_bcs.extend(symmetry_bcs)
        
        print_message("Creating Neumann BCs.")
        L = Constant(0)*q*dx + self.p_neumann_terms(ds, dot(v, n),
                                                    self.p_neumann_subdomains)

        if self._matrices is None:
            print_message("Assembling matrices.")
            (A, bb) = assemble_system(a, L, dirichlet_bcs)
            (P, _) = assemble_system(b, L, dirichlet_bcs)
            self._matrices = (A, P)
        else:        
            print_message("Using stored matrices.")
            A, P = self._matrices
            bb = assemble(L)
            for bc in dirichlet_bcs:
                bc.apply(A, bb)   # note to self: check this
                


        solver = PETScKrylovSolver(self.solver, self.preconditioner)
        if self.solver == "gmres":
            solver.ksp().setGMRESRestart(self.gmres_restarts)


        solver.set_operators(A, P)

            
            
            
        kpars = parameters["krylov_solver"]
        kpars["absolute_tolerance"] = self.atol # default: 1E-15
        kpars["relative_tolerance"] = self.rtol # default: 1E-9
        kpars["maximum_iterations"] = self.max_its # default: ?

        if not self.suppress_debug:
            kpars["report"] = True
            kpars["monitor_convergence"] = True
            
        if self.initial_guess is not None:
            print_message("Using nonzero initial guess.")
            kpars["nonzero_initial_guess"] = True
            w2 = interpolate(self.initial_guess, self.W)
            w.vector()[:] = w2.vector()[:]
            
        solver.update_parameters(kpars)
        
        print_message("Starting solve.")
        solver.ksp().setConvergenceHistory()
        num_its = solver.solve(w.vector(), bb)
        self.residuals = solver.ksp().getConvergenceHistory()
        print_message("Solving complete in {} iterations.".format(num_its))

        u, p = w.split()
        
        return u, p

    def avg_velocity(self, u):
        dx = self.domain.measure("dx")
        # import IPython; IPython.embed()
        vel_int = assemble(sqrt(dot(u, u))*dx)
        volume = assemble(1*dx)
        return vel_int/volume, volume
