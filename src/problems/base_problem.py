from dolfin import MPI, mpi_comm_world, Function


class BaseProblem(object):
    def is_primary_process(self):
        return MPI().rank(mpi_comm_world()) == 0

    def num_dofs(self, fspace=None):
        if fspace is None:
            fspace = self.W
        w = Function(fspace)
        return w.vector().array().shape[0]

