from dolfin import (FacetFunction, TrialFunction, Function, Mesh,
                    FiniteElement, interpolate,
                    tetrahedron, triangle, FunctionSpace, TestFunction,
                    inner, grad, dx, div, dot, Expression, Constant, FacetNormal,
                    DirichletBC, near, solve, assemble, avg, Measure, assemble_system, 
                    parameters, KrylovSolver,
                    File, plot, interactive,
                    MPI, mpi_comm_world, mpi_comm_self
                    )
import numpy as np
from base_problem import BaseProblem
from utils import print_message



class TimeDependentPoissonProblem(BaseProblem):

    def __init__(self, argdict, domain,
                 dirichlet_subdomains, u_init=None):
        self.atol = argdict["atol"]
        self.rtol = argdict["rtol"]
        self.solver = argdict["solver"]
        self.preconditioner = argdict["preconditioner"]
        self.elt_degree = argdict["elt_degree"]
        self.max_its = argdict["max_its"]
        
        self.random_guess = argdict["random_guess"]
        self.suppress_debug = argdict["suppress_debug"]

        self.domain = domain
        
        self.W = self.function_space()

        self.dirichlet_subdomains = dirichlet_subdomains
        self.A = None

        if u_init is None:
            self.u_curr = Function(self.W)
        else:
            self.u_curr = interpolate(u_init, self.W)

        self.dt = argdict["dt"]
        self.D = argdict["D"]


        


    @staticmethod        
    def parameters():
        """Expected arguments and descriptions."""
        args = {
            "atol": ("Absolute tolerance.", 1E-9),
            "rtol": ("Relative tolerance.", 1E-9),
            "random_guess": ("Use random intial guess for iterative solver.", False),
            "suppress_debug": ("Suppress debugging output.", False),
            "elt_degree": ("Degree of elements.", 2),
            "preconditioner": ("Preconditioner.", "cg"),
            "solver": ("Solver.", "hypre_amg"),
            "D": ("Diffusion coefficient.", 0.1),
            "dt": ("timestep.", 0.1)
        }
        return args


    def function_space(self):
        print_message("Creating FunctionSpace of degree {}.".format(self.elt_degree))

        elt = self.domain.mesh.ufl_cell()
            
        Pn = FiniteElement("Lagrange", elt, self.elt_degree)
        return FunctionSpace(self.domain.mesh, Pn)
        # return FunctionSpace(self.domain.mesh, "CG", 1)

    
    def dirichlet_bcs(self, bcs={}):
        # dirichlet_subdomains should hold pairs (n: val)
        # where val is the value of the solution at subdomain n
        # as a FEniCS expression

        return [DirichletBC(self.W, bcs[subdomain_id],
                            self.domain.subdomain_data, subdomain_id)
                for subdomain_id in bcs]



    
    def setup(self):
        
        print_message("Creating Test/TrialFunctions.")
        u = TrialFunction(self.W)
        v = TestFunction(self.W)
        

        print_message("Creating Dirichlet BCs.")
        self.bc_list = self.dirichlet_bcs(self.dirichlet_subdomains)
        
            
        self.a = v * u * dx + self.D * self.dt * inner(grad(u), grad(v))*dx
        
        self.L = v * self.u_curr * dx            

 
        print_message("Assembling matrices.")
        self.A = assemble(self.a)
        # self.P = assemble(p)
        
        
        kpars = parameters["krylov_solver"]
        kpars["absolute_tolerance"] = self.atol # default: 1E-15
        kpars["relative_tolerance"] = self.rtol # default: 1E-9
        kpars["nonzero_initial_guess"] = True
        kpars["maximum_iterations"] = self.max_its # default: ?
        
        if not self.suppress_debug:
            kpars["report"] = True
            kpars["monitor_convergence"] = True
            
        self.ksolver = KrylovSolver(self.solver, self.preconditioner)
        self.ksolver.set_operator(self.A)#, self.A)
        self.ksolver.update_parameters(kpars)
        self.u_next = Function(self.W)

        a = v * u * dx
        L = v * self.u_curr * dx
        solve(a == L, self.u_next, self.bc_list)
        self.u_curr.assign(self.u_next)

    def next(self, print_msg=False):
        self.bb = assemble(self.L)
        for bc in self.bc_list:
            bc.apply(self.A, self.bb)

        if print_msg:
            print_message("Starting solve with {} and {}.".format(self.solver,
                                                                  self.preconditioner))
        


        num_its = self.ksolver.solve(self.u_next.vector(), self.bb)
        # solve(self.A, self.u_next.vector(), self.bb)
        # print_message("Solving complete in {} iterations.".format(num_its))
        # solve(self.a == self.L, w, self.bc_list)
        self.u_curr.assign(self.u_next)
        

