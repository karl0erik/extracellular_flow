import sys, os
import numpy as np
from dolfin import *
from problems import StokesProblem
from utils import default_arg_dict, print_message, is_primary_process
from domains import KinneyDomain, BoxDomain, ParallelOctagonalHoleDomain
from itertools import product

"""Solve a Stokes problem repeatedly on cutout of Kinney ECS mesh.""" 




mesh_fn = sys.argv[1]           # will be loaded by dolfin as Mesh(mesh_fn)
output_fn = sys.argv[2]         # 

output_dir = "refinement_check_output"
if is_primary_process():
    try:
        os.makedirs(output_dir)
    except OSError, e:
        pass
    try:
        with open(output_fn, "w") as f:
            f.write("Output file for permeability\n\n")
    except OSError, e:
        pass

solver = "minres"
preconditioner = "hypre_amg"

gmres_restarts = 50
max_its = int(sys.argv[3])


if len(sys.argv) > 4:
    flow_direction = sys.argv[4]
else:
    flow_direction = "x"

print_message("Using flow direction " + flow_direction)

p_argdict, d_argdict = map(default_arg_dict, [StokesProblem,
                                              KinneyDomain])

d_argdict["near_eps"] = 5.                                              

# plot(domain.subdomain_data)
# interactive()
# import IPython; IPython.embed()
# sys.exit(1)



p_argdict.update(
    {
        "atol": 5E-7, "rtol": 5E-7,
        "random_guess": False,
        "suppress_debug": False,
        "debug_mode": False,
        "solver": solver,
        "preconditioner": preconditioner,
        "gmres_restarts": gmres_restarts,
        "max_its": max_its,
        "use_P1P1": False
    }
)


# for mu, ppx in product([0.8, 0.8E-3], [133E-3, 133E-6]):
for mu, ppx in product([0.8E-3], [133E-6]):
    if is_primary_process():
        with open(output_fn, "a") as f:
            f.write("\n{}\nTesting with mu={}, ppx={}\n\n".format("-"*40, mu, ppx))
            
    p_argdict["mu"] = mu

    ## BCs
    zero = Constant((0, 0, 0))
    if flow_direction == "x":
        dirichlet_bcs = {
            i: zero
            for i in range(1, 8)
            if not((i == 2) or (i == 3))
        }
        initial_guess = Expression(("0", "0", "0",
                                    "x[0] * dp"), dp=ppx, degree=1)
        symm_bcs = {4: 1, 5:1, 6:2, 7:2}
    elif flow_direction == "y":
        dirichlet_bcs = {
            i: zero
            for i in range(1, 8)
            if not((i == 4) or (i == 5))
        }
        initial_guess = Expression(("0", "0", "0",
                                    "x[1] * dp"), dp=ppx, degree=1)
        symm_bcs = {2: 0, 3:0, 6: 2, 7:2}
    elif flow_direction == "z":                           
        dirichlet_bcs = {
            i: zero
            for i in range(1, 8)
            if not((i == 6) or (i == 7))
        }
        initial_guess = Expression(("0", "0", "0",
                                    "x[2] * dp"), dp=ppx, degree=1)
        symm_bcs = {2: 0, 3:0, 4: 1, 5: 1}
    else:
        print_message("can't use flow direction {}".format(flow_direction))
        sys.exit(1)


    p_bc = initial_guess[3]

    if True:                        # hacky way of toggling between dirichlet/symm bcs
        dirichlet_bcs = {1: zero}
    else:
        symm_bcs = {}

    p_argdict["initial_guess"]=initial_guess
    neumann_bcs = {i: p_bc for i in range(1, 8)} # set presure everywhere

    domain = KinneyDomain(d_argdict, mesh_fn)

    times_to_refine = 1
    for n in range(times_to_refine + 1):

        ## problem
        problem = StokesProblem(p_argdict, domain,
                                dirichlet_bcs, neumann_bcs, symm_bcs)

        u, p = problem.solve()
        if is_primary_process():
            np.save("{}/beta_{}".format(output_dir, problem.beta), problem.residuals)

        f = File("{}/u_{}_times_refined.pvd".format(output_dir, n))
        f << u

        h = File("{}/p_{}_times_refined.pvd".format(output_dir, n))
        h << p
        
        # g = File("bdy_parts.pvd")
        # g << domain.subdomain_data

        xmin, xmax, ymin, ymax, zmin, zmax = domain.get_coord_minmax(domain.mesh)
        Lx, Ly, Lz = xmax - xmin, ymax - ymin, zmax - zmin
        A = (ymax - ymin) * (zmax - zmin)
        mu = problem.mu
        ds = domain.measure("ds")
        dx = domain.measure("dx")

        if flow_direction == "x":
            middle_coord = (xmin + xmax)/2

            flow_ind = 0
            L = Lx
            A = Ly*Lz
        elif flow_direction == "y":
            middle_coord = (ymin + ymax)/2
            flow_ind = 1
            L = Ly
            A = Lx * Lz
        else:                       # = z
            middle_coord = (zmin + zmax)/2
            L = zmin - zmax
            A = Lx * Ly

        class OnlyMiddle(Expression): # 1 in middle 10%, otherwise 0
            def eval(self, v, x):
                if abs(x[flow_ind] - middle_coord)/L < 0.05:
                    v[0] = 1
                else:
                    v[0] = 0

        m = OnlyMiddle(degree=0)

        middle_vol = assemble(m*dx)
        middle_vol = 0.1*L*A
        # import IPython; IPython.embed()
        v = assemble(u[flow_ind]*m*dx)/middle_vol

        kappa = v*mu/ppx
        print "Permability at refinement level {}: {}".format(n, kappa)
        if is_primary_process():
            with open(output_fn, "a") as f:
                f.write("Permability at level {}: {}\n".format(n, kappa))


        
        if n != times_to_refine:
            domain.refine()






