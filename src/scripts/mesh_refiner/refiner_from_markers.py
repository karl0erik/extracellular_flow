from dolfin import *
import sys

mesh_fn = sys.argv[1]
refine_markers_fn = sys.argv[2]
out_mesh_fn = sys.argv[3]
out_file = File(out_mesh_fn)

mesh = Mesh(mesh_fn)
print "Mesh loaded"
refine_markers = MeshFunction("bool", mesh, refine_markers_fn)
print "Refine markers loaded"
out_file << refine(mesh, refine_markers)
print "Done refining!"
