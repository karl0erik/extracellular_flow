#include <dolfin.h>
// #include "Poisson.h"
#include <iostream>
using namespace dolfin;

// // Source term (right-hand side)
// class Source : public Expression
// {
//   void eval(Array<double>& values, const Array<double>& x) const
//   {
//     double dx = x[0] - 0.5;
//     double dy = x[1] - 0.5;
//     values[0] = 10*exp(-(dx*dx + dy*dy) / 0.02);
//   }
// };

// // Normal derivative (Neumann boundary condition)
// class dUdN : public Expression
// {
//   void eval(Array<double>& values, const Array<double>& x) const
//   {
//     values[0] = sin(5*x[0]);
//   }
// };

// // Sub domain for Dirichlet boundary condition
// class DirichletBoundary : public SubDomain
// {
//   bool inside(const Array<double>& x, bool on_boundary) const
//   {
//     return x[0] < DOLFIN_EPS or x[0] > 1.0 - DOLFIN_EPS;
//   }
// };

// return_type function_name( parameter list )
// {
//    body of the function
// }

bool is_exterior_vertex(Vertex vertex)
{
  for (FacetIterator face(vertex); !face.end(); ++face){
    if (face->exterior()){
      return true;
    }
  }
  return false;
}

bool is_interior_edge(Edge edge)
{
  for (FacetIterator face(edge); !face.end(); ++face){
    if (face->exterior()){
      return false;
    }
  }

  return true;
}


int main(int argc, char* argv[])
{
  // argv[1] = input filename
  // argv[2] = output filename

  // std::shared_ptr<const Mesh>
  auto mesh = std::make_shared<Mesh>(argv[1]);  

  mesh->init(2, 3);
  mesh->init(1, 2);
  mesh->init(0, 1);

  // mark all cells containing two exterior vertices joined by an interior edge
  auto cell_markers = std::make_shared<MeshFunction<bool>>(mesh, 3, false);

  for (CellIterator cell(*mesh); !cell.end(); ++cell)
    {
      for (EdgeIterator edge(*cell); !edge.end(); ++edge){

	if(is_interior_edge(*edge)){
	  bool bad_edge = true;
	  for(VertexIterator vertex(*edge); !vertex.end(); ++vertex){
	    if(not is_exterior_vertex(*vertex)){
	      bad_edge = false;
	      break;
	    }	    
	  }
	  if(bad_edge){
	    cell_markers->set_value(cell->index(), true);
	    break;
	  }
	}
      }
  
    }
  if(argc > 2){			// save refinement markers
    File markers_file(argv[2]);
    markers_file << *cell_markers;
  }  
  
  if(argc > 3){			// actually refine mesh
    File refined_mesh_file(argv[3]);
    refined_mesh_file << refine(*mesh, *cell_markers);
  }

  return 0;
}
