import sys, glob
from subprocess import call
from os import system
from math import sqrt

# helper script for computing number of iterations required as function of mesh resolution

def visualize(geom):
    with open("report_{}_mr_comparison.txt".format(geom), "r") as report_f:
        lines = report_f.readlines()
        mrs = map(lambda s: s.split(": ")[0], lines)
        its = map(lambda s: s.split(": ")[1], lines)
        import matplotlib.pyplot as plt

        mrs_filtered = []
        its_filtered = []
        for n in range(len(mrs)):
            if its[n] != "ERROR\n":
                its_filtered.append(int(its[n]))
                mrs_filtered.append(int(mrs[n]))

        mrs, its = mrs_filtered, its_filtered
        print mrs, its
        plt.plot(mrs, its)
        plt.ylim(0, 1.1*max(its))
        plt.xlim(min(mrs), max(mrs))
        plt.show()
        
def generate_data(geom):
    for mesh_resolution in [int(10 * 1.2**n) for n in range(10)]:
    # for mesh_resolution in range(20, 201, 20):
    # for mesh_resolution in [int(5 * n**2) for n in range(2, 8)]:
        command = (
            "python cli.py {geom} solve -atol 1E-7 -rtol 1E-7 -mesh_resolution {mr} --random_guess "
            "-output_folder {fld} > output_{geom}_{mr}.txt").format(
                mr=mesh_resolution, geom=geom,
                fld="{}_mr_test/mr_".format(geom) + str(mesh_resolution)
            )
        print command
        call(command, shell=True)

def generate_report(geom):
    def fn_to_mr(fn):
        return int(fn[len("output_{}_".format(geom)):-4])
                   
    with open("report_{}_mr_comparison.txt".format(geom), "w") as report_f:
        for output_fn in sorted(glob.glob("output_{}_*.txt".format(geom)),
                                key=fn_to_mr):
            mesh_resolution = fn_to_mr(output_fn)
            with open(output_fn, "r") as output_f:
                lines = output_f.readlines()
                if not "complete." in lines[-1]:
                    report_f.write("{mr}: ERROR\n".format(mr=mesh_resolution))
                else:
                    report_f.write("{mr}: {num_iters}\n".format(mr=mesh_resolution,
                                                                num_iters=lines[-2].split()[0]))

if __name__ == "__main__":
    if len(sys.argv) > 2:
        geom = sys.argv[2]
        if sys.argv[1] == "viz":
            visualize(geom)
        elif sys.argv[1] == "rep":
            generate_report(geom)
        elif sys.argv[1] == "gen":
            generate_data(geom)
        else:
            print "what is ", sys.argv[1]
    else:
        geom = sys.argv[1]
        generate_data(geom)
        generate_report(geom)
        visualize(geom)
