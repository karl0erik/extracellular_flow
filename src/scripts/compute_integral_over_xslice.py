#### import the simple module from the paraview
from paraview.simple import *
import sys, time


### takes a bunch of .pvd-files as command line arguments, opens them, puts a 
### slice in the YZ plane in hopefully a nice spot, computes the integral of flow
### through that plane and saves the result to file 

start_time = time.time()
def print_message(msg):
    print "[{:6d}s]".format(int(time.time() - start_time)), msg


print "Processing the following files:"
print sorted(sys.argv[1:])
for u_pvd_fn in sorted(sys.argv[1:]):
    assert u_pvd_fn[-4:] == ".pvd"
    output_csv_fn = u_pvd_fn[:-4] + ".csv"

    print_message("  Processing file {}".format(u_pvd_fn))

    u_pvd = PVDReader(FileName=u_pvd_fn)
    

    print_message("    Creating slice filter")
    slice1 = Slice(Input=u_pvd)
    slice1.SliceType = 'Plane'
    slice1.SliceOffsetValues = [0.0]

    # init the 'Plane' selected for 'SliceType'
    slice1.SliceType.Origin = [3320.0, 3570.0, 3725.0]

    print_message("    Creating integrate variables filter")
    integrateVariables1 = IntegrateVariables(Input=slice1)

    ## doing below 2 lines uses cell data instead , but should not matter (except saving needs to be changed)
    # pointDatatoCellData1 = PointDatatoCellData(Input=slice1)
    # integrateVariables1 = IntegrateVariables(Input=pointDatatoCellData1)


   
    print_message("    Computing result and saving point data to file {}".format(output_csv_fn))

    #doesn't work in paraview 3.98 on Abel
    # SaveData(output_csv_fn, proxy=integrateVariables1, FieldAssociation='Points') n


    writer = CreateWriter(output_csv_fn, integrateVariables1)
    writer.FieldAssociation = "Points"
    writer.UpdatePipeline() # actually writes
    print_message("    Contents of file {}:".format(output_csv_fn))
    with open(output_csv_fn, 'r') as f:
        print f.read()
    
