from subprocess import call
from os import system
from math import sqrt

if __name__ == "__main__":
    cell_diam = 80
    mesh_resolution = 200
    vf = 0.2
    length = 15
    
    for T in [2, 3.5, 5, 6.5, 8]:
        # computes C such that ratio interstitial volume / total box volume = vf
        
        corner_size = sqrt((cell_diam**2 - (1 - vf) * (cell_diam + T)**2)/2)

        command = (
            "python cli.py symmetric solve -cell_diam {S} -length {L}"
            " -tunnel_width {T} -corner_size {C} -mesh_resolution {mr} "
            " -solver tfqmr -output_folder {fld} --suppress_debug").format(
                C=corner_size, S=cell_diam, T=T, L=length, mr=mesh_resolution, 
                fld="tunnel_comparison/T_" + str(T)
            )
        print command
        call(command, shell=True)
