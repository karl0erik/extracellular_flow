import sys, glob, time
from subprocess import call




if __name__ == "__main__":
    time_spent_list = []
    for np in range(1, 5):
        NUM_TESTS = 30
        time_spent = 0
        start_time = time.time()
        
        for _ in range(NUM_TESTS):
            command = ("mpirun -np {} python cli.py flat_octagonal_holes"
                       " solve -mesh_resolution 80 --random_guess --suppress_debug").format(np)
            call(command, shell=True)

        time_spent += time.time() - start_time
        time_spent_list.append((time.time() - start_time)/float(NUM_TESTS))
    print time_spent_list
