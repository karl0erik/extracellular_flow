import sys, os
import numpy as np
import pickle
from dolfin import *
from problems import StokesProblem
from utils import default_arg_dict, print_message, is_primary_process
from domains import *

set_log_level(40)

output_dir = "hexagon_output_files"
if is_primary_process():
    try:
        os.makedirs(output_dir)
    except OSError, e:
        pass

use_TH = len(sys.argv) > 2


solver = "minres"
preconditioner = "hypre_amg"
print_message("Using solver {} and preconditioner {}".format(solver, preconditioner))
gmres_restarts = 50
max_its = 10000

db_mode = False


domain_type = RandomlyRoundedHexagonalHoleDomain
p_argdict, d_argdict = map(default_arg_dict, [StokesProblem,
                                              domain_type])

def w_vf_tvf_constraint(w, vf, tvf):
    """Computes parameters so that volume fraction, tunnel 
    volume fraction and sheet width are the given values"""
    
    
    return w, r, a

def normal_vf(w, r, a):
    return  (4*sqrt(3)*(r**2*(-pi/2 + sqrt(3))
                        + sqrt(3)*r*w + sqrt(3)*w**2/4
                        + 3*w*(a/2 - sqrt(3)*r/3))
             /(3*(sqrt(3)*a + w)**2))

def tunnel_vf(w, r, a):
    return ((-4*sqrt(3)*r**2 + 2*pi*r**2 - 4*sqrt(3)*r*w - sqrt(3)*w**2)
            /(-6*a*w - 4*sqrt(3)*r**2 + 2*pi*r**2 - sqrt(3)*w**2))


def w_w1_vf_constraint(w, w1, vf):
    r = (w1 - w)/(2 - sqrt(3))      # necessary to get tunnel width 60 as in Kinney
    
    a = (-sqrt(3)*w*(vf - 1)
        + sqrt(-2*sqrt(3)*pi*vf*r**2
               + 12*vf*r**2
               - 3*vf*w**2
               + 3*w**2))/(3*vf)  # black magic to make vf right
    return w, r, a
    

# w, r, a = w_w1_vf_constraint(25, 40, 0.1)
# w, r, a = w_vf_tvf_constraint(25, 0.1, 0.33)

# print normal_vf(w, r, a)

def compute_permeability(w, r, a):
    r_max = a * sqrt(3)/2
    alpha = r/r_max
    beta = 1 - alpha

    
    p_argdict, d_argdict = map(default_arg_dict, [StokesProblem,
                                                  domain_type])
    d_argdict.update(
        {
            "resolution": 1000.0,
            "Nz": 1,
            "Ny": 1,
            "n_round": 10,
            "sheet_width": w,
            "side_length": a,
            "length": 20.,
            "alpha": alpha,
            "beta": beta,
            "deterministic": True,

            "seed": None,

        }
    )
    p_argdict.update(
        {
            "atol": 5E-7, "rtol": 5E-7,
            "random_guess": False,
            "suppress_debug": True,
            "debug_mode": db_mode,
            "mu": 0.8E-3,
            # "mu": 1.0,           
            "solver": solver,
            "preconditioner": preconditioner,
            "gmres_restarts": gmres_restarts,
            "max_its": 4000,
            "beta": 0.2,
            "use_P1P1": not use_TH
        }
    )

    domain = domain_type(d_argdict)
    f = File("tmp.pvd")
    f << domain.mesh
    
    zero = Constant((0, 0, 0))
    dirichlet_bcs = {
        1: zero
    }

    symmetry_bcs = {
        4: 1, 5:1,                  # subdom_id: coord of normal to subdom_id
        6: 2, 7:2
    }


    ppx = 133E-6 # pressure differential of 1 mmHg/mm (in Pa/nanometer)
    initial_guess = Expression(("0", "0", "0",
                                "x[0] * dp"), dp=ppx, degree=1)

    p_bc = initial_guess[3] #3D

    p_argdict["initial_guess"]=initial_guess

    neumann_bcs = {i: p_bc for i in [2, 3]} # set pressure only on inflow/outflow

    ## problem
    problem = StokesProblem(p_argdict, domain,
                            dirichlet_bcs, neumann_bcs, symmetry_bcs)

    u, p = problem.solve()
    xmin, xmax, ymin, ymax, zmin, zmax = domain.get_coord_minmax(domain.mesh)
    A = (ymax - ymin) * (zmax - zmin)
    mu = p_argdict["mu"]

    ds = domain.measure("ds")
    outflux = assemble(u[0]*ds(3))
    v = outflux/A

    return v * mu / ppx

w_const = 25
scale_factor = 400
results = {}

# compute_permeability(25, 33.5884572681, 271.856118616)

# sys.exit(0)
for w1 in np.linspace(w_const, 200, 80):
    w, r, a = w_w1_vf_constraint(w_const, w1, 0.1)
    r_max = a * sqrt(3)/2
    if w <= 0 or r <= 0 or a <= 0 or r >= r_max:
        continue#raise Exception()

    # rescale so that a = 100
    W, R, A = w/a, r/a, a/a
    w, r, a = W*scale_factor, R*scale_factor, A*scale_factor
    print w, r, a
    try:
        kappa = compute_permeability(w, r, a)
    except:
        print "skipping"
        continue
    results[w1] = w, r, a, kappa
    print ("w: {:.2f},  r:{:.2f}, a:{:.2f},"
           " vf:{:.2f}, tvf:{:.2f}, k:{:.2f}").format(w, r, a,
                                                      normal_vf(w, r, a),
                                                      tunnel_vf(w, r, a), kappa)
    

    
print w, r, a

print "-"*80
f = open("output_files/hexacomp_results.pickle", "w")
pickle.dump(results, f)
f.close()
for w1 in results:
    w, r, a, kappa = results[w1]
    print ("w: {:7.2f},  r:{:7.2f}, a:{:7.2f},"
           " vf:{:7.2f}, tvf:{:7.2f}, k:{:7.2f}").format(w, r, a,
                                                         normal_vf(w, r, a),
                                                         tunnel_vf(w, r, a), kappa)

# print ("w: {:7.2f},  r:{:7.2f}, a:{:7.2f},"" vf:{:7.2f}, tvf:{:7.2f}, k:{:7.2f}").format(w, r, a,normal_vf(w, r, a), tunnel_vf(w, r, a), kappa)

