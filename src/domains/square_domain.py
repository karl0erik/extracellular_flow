from base_domain import BaseHoledBoxDomain
from dolfin import Point
from mshr import Rectangle
import time

class SquareDomain(BaseHoledBoxDomain):

    dimension = 2
    
    def __init__(self, argdict):
        super(SquareDomain, self).__init__(argdict)
        self.side_length = argdict["side_length"]


    def domain_description(self, unit="microns"):
        return (
            "A square of side length {s} {u}. Mesh has resolution {res}."
        ).format(s=self.side_length, res=self.mesh_resolution, u=unit)
    @staticmethod
    def parameters():
        par_dict = super(SquareDomain, SquareDomain).parameters()
        par_dict.update(
            {
                "side_length": ("Side length of square", 30.0)
            }
        )
        return par_dict


    def create_domain(self):
        S = self.side_length
        domain = Rectangle(Point(0, 0), Point(S, S))

        return domain

