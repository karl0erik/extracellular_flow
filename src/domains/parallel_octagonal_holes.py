from base_domain import BaseHoledBoxDomain
from dolfin import Point
from math import pi
from mshr import (Box, CSGTranslation, CSGRotation, generate_mesh,
                  Extrude2D, Polygon)

class ParallelOctagonalHoleDomain(BaseHoledBoxDomain):
    dimension = 3
    
    def __init__(self, argdict):
        super(ParallelOctagonalHoleDomain, self).__init__(argdict)

        self.length = argdict["length"]
        self.tunnel_width = argdict["tunnel_width"]
        self.cell_diam = argdict["cell_diam"]
        self.C = argdict["corner_size"]
        self.S = self.cell_diam - 2*self.C
        if self.S <= 0:
            raise Exception("cell diameter must be > 2 * corner size")

        self.Nz = argdict["Nz"]
        self.Ny = argdict["Ny"]
        self.resolution = argdict["resolution"]

    def description(self, unit="microns"):
        return (
            " A rectangular tube of length {l} with a grid of "
            " {Nz} by {Ny} octagonal holes going parallel to the tube "
            " of width {cell_diam} microns and corner size {C} microns separated "
            "by a distance of {T} microns. Mesh has resolution {res} was used."
        ).format(Nz=self.Nz, Ny=self.Ny,
                 cell_diam=self.cell_diam, C=self.C, l=self.length,
                 T=self.tunnel_width, res=self.mesh_resolution
        )


        
    @staticmethod
    def parameters():
        par_dict = super(ParallelOctagonalHoleDomain, ParallelOctagonalHoleDomain).parameters()
        par_dict.update(
            {
                "Nz": ("Number of tubular holes in z-direction.", 2, int),
                "Ny": ("Number of tubular holes in y-direction.", 1, int),
                "tunnel_width": ("Distance between tubular holes in microns.", 5, float),
                "corner_size": ("Corner size of tubular holes in microns.", 2, float),
                "cell_diam": ("Diameter of tubular holes in microns.", 30, float),
                "length": ("Length of tube in microns.", 30, float)
            }
        )
        return par_dict

    
    def create_domain(self):
        S, C, T, l = map(float, [self.S, self.C, self.tunnel_width, self.length])
        Nz, Ny = self.Nz, self.Ny
        octagon_vertex_coords = [
            (C, 0),
            (S+C, 0),
            (S+2*C, C),
            (S+2*C, S+C),
            (S+C, S+2*C),
            (C, S+2*C),
            (0, S+C),
            (0, C)
        ]

        octagon_vertices = [Point(*coords)
                            for coords in octagon_vertex_coords]

        octagon = Polygon(octagon_vertices)
        octagon_tube = CSGRotation(Extrude2D(octagon, l),
                                   Point(0, 1, 0),
                                   Point(C + S/2, 0, C + S/2), -pi/2)

        translate_length = self.cell_diam + T
        holes = [CSGTranslation(octagon_tube, Point(0, n*translate_length,
                                                    m*translate_length))
                 for n in range(Ny)
                 for m in range(Nz)]


        eps = 1E-4
        self.xmin = 0 + eps
        self.xmax = l * (1 - eps)
        self.ymin = -T/2 * (1 - eps)
        self.ymax = (Ny*translate_length + self.ymin) * (1 - eps)
        self.zmin = -T/2 * (1 - eps)
        self.zmax = (Nz*translate_length + self.zmin) * (1 - eps)
        
        domain = Box(Point(self.xmin, self.ymin, self.zmin),
                     Point(self.xmax, self.ymax, self.zmax))

        for hole in holes:
            domain = domain - hole

        return domain
