from dolfin import SubDomain, FacetFunction, near, assemble, Measure, refine
from mshr import TetgenMeshGenerator3D, CSGCGALDomain3D, generate_mesh
from utils import print_message, load_mesh


class BaseDomain(object):
    
    """
    Template for domains.
    """

    # dimension = ?

    def __init__(self, arg_dict):
        self.resolution = arg_dict["resolution"]
        self.debug=arg_dict["debug"]
        self.near_eps=arg_dict["near_eps"]
        if self.debug:
            print "Debug mode enabled"

    @staticmethod
    def parameters():
        return {
            "resolution": ("Resolution to use when generating mesh.", 20.0),
            "debug": ("Debugging flag for exciting output.", False),
            "near_eps": ("Tolerance for partitioning boundary.", 1E-3)
        }
    
    def create_domain(self):
        """Returns a mshr domain which can be passed to mesh generator."""
        raise NotImplementedError()
    
    def partition_boundary(self):
        """Returns a FacetFunction labeling the subdomains with ints."""
        raise NotImplementedError()

    def domain_description(self, unit="microns"):
        """Return a brief text description of the domain."""
        return "A domain with distances measured in {u}.".format(unit)

    def get_coord_minmax(self, mesh):
        coords = mesh.coordinates()
        xcoords, ycoords = coords[:, 0], coords[:, 1]
        xmin = min(xcoords)
        xmax = max(xcoords)
        ymin = min(ycoords)
        ymax = max(ycoords)
        
        if self.dimension == 3:
            zcoords = coords[:, 2]
            zmin = min(zcoords)
            zmax = max(zcoords)

        if self.dimension == 3:
            return [xmin, xmax, ymin, ymax, zmin, zmax]
        else:
            return [xmin, xmax, ymin, ymax]

    def create_mesh(self):
        domain = self.create_domain()
        if self.dimension == 2:
            self._mesh = generate_mesh(domain, int(self.resolution))
        else:
            generator = TetgenMeshGenerator3D() 
            generator.parameters["mesh_resolution"] = self.resolution
            self._mesh = generator.generate(CSGCGALDomain3D(domain))

        return self._mesh

    @property
    def mesh(self):
        try:
            return self._mesh
        except AttributeError:
            print_message("Starting mesh creation.")
            self._mesh = self.create_mesh()
            
            return self._mesh

    @property
    def subdomain_data(self):
        try:
            return self._subdomain_data
        
        except AttributeError:
            print_message("Paritioning boundary.")
            self._subdomain_data = self.partition_boundary()
            return self._subdomain_data
    

    def measure(self, measure_type):
        return Measure(measure_type, domain=self.mesh,
                       subdomain_data=self.subdomain_data)

    def refine(self):
        refined_mesh = refine(self.mesh)
        del self._mesh               # self._mesh exists from previous line
        if hasattr(self, "_subdomain_data"):
            del self._subdomain_data
        self._mesh = refined_mesh
        
class FileDomain(BaseDomain):
    def __init__(self, argdict, fn):
        super(FileDomain, self).__init__(argdict)
        self.filename = fn

    def create_mesh(self):
        return load_mesh(self.filename)
        
class BaseHoledBoxDomain(BaseDomain):

    """
    Base class for domains which look like a box with some kind of holes.
    Box has edges parallel to the coordinate axes.
    """
    
    def partition_boundary(self):
        
        boundary_parts = FacetFunction("size_t", self.mesh)
        
        if self.dimension == 2:
            xmin, xmax, ymin, ymax = self.get_coord_minmax(self.mesh)
        else:
            xmin, xmax, ymin, ymax, zmin, zmax = self.get_coord_minmax(self.mesh)

        near_eps = self.near_eps
        
        class HoleBoundary(SubDomain): 
            def inside(self, x, on_boundary):
                # hard to specify hole boundaries, so mark everything
                # and then re-mark that which is not a hole
                return on_boundary
        HoleBoundary().mark(boundary_parts, 1)

        class LeftBoundary(SubDomain): 
            def inside(self, x, on_boundary):
                return (near(x[0], xmin, eps=near_eps) and on_boundary)

        LeftBoundary().mark(boundary_parts, 2)

        class RightBoundary(SubDomain): 
            def inside(self, x, on_boundary):
                return (near(x[0], xmax, eps=near_eps) and on_boundary)

        RightBoundary().mark(boundary_parts, 3)

        
        class BackBoundary(SubDomain):
            def inside(self, x, on_boundary):
                return (near(x[1], ymax, eps=near_eps) and on_boundary)

        BackBoundary().mark(boundary_parts, 4)
        
        class FrontBoundary(SubDomain):
            def inside(self, x, on_boundary):
                return (near(x[1], ymin, eps=near_eps) and on_boundary)

        FrontBoundary().mark(boundary_parts, 5)


        if self.dimension == 3:
            class TopBoundary(SubDomain):
                def inside(self, x, on_boundary):
                    return (near(x[2], zmax, eps=near_eps) and on_boundary)

            TopBoundary().mark(boundary_parts, 6)


            class BotBoundary(SubDomain):
                def inside(self, x, on_boundary):
                    return (near(x[2], zmin, eps=near_eps) and on_boundary)

            BotBoundary().mark(boundary_parts, 7)

        self._subdomain_data = boundary_parts
        return self._subdomain_data

    
