from base_domain import FileDomain
from dolfin import Mesh, XDMFFile, mpi_comm_world,  SubDomain, FacetFunction, near

    
class ZebrafishDomain(FileDomain):
    dimension = 3
    

    def partition_boundary(self):
        """Returns a FacetFunction labeling the subdomains with ints."""

        ## boundary splits into:
        # a bunch of vessel openings, each parallel with a coordinate plane
        # everything else: vessel walls


        boundary_parts = FacetFunction("size_t", self.mesh)
        
        # xmin, xmax, ymin, ymax, zmin, zmax = self.get_coord_minmax(self.mesh)

        near_eps = 0.4
        
        class VesselWall(SubDomain): 
            def inside(self, x, on_boundary):
                # hard to specify hole boundaries, so mark everything
                # and then re-mark that which is not a hole
                return on_boundary
        VesselWall().mark(boundary_parts, 1)

        class VesselInflow1(SubDomain): 
            def inside(self, x, on_boundary):
                return (near(x[0], 602, eps=near_eps) and on_boundary)

        VesselInflow1().mark(boundary_parts, 2)

        class VesselInflow2(SubDomain): 
            def inside(self, x, on_boundary):
                return (near(x[0], 711, eps=near_eps) and on_boundary)

        VesselInflow2().mark(boundary_parts, 3)


        class VesselOutflow1(SubDomain): 
            def inside(self, x, on_boundary):
                return (near(x[1], 293, eps=near_eps)
                        and (x[0] < 657) and on_boundary)

        VesselOutflow1().mark(boundary_parts, 4)

                
        class VesselOutflow2(SubDomain): 
            def inside(self, x, on_boundary):
                return (near(x[0], 648, eps=near_eps)
                        and (x[1] > 320) and on_boundary)

        VesselOutflow2().mark(boundary_parts, 5)


        class VesselOutflow3(SubDomain): 
            def inside(self, x, on_boundary):
                return (near(x[0], 708, eps=near_eps)
                        and (x[1] > 320) and on_boundary)

        VesselOutflow3().mark(boundary_parts, 6)


        self._subdomain_data = boundary_parts
        return self._subdomain_data

    


