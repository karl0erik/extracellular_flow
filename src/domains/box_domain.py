from base_domain import BaseHoledBoxDomain
from dolfin import Point
from mshr import Box
import time

class BoxDomain(BaseHoledBoxDomain):

    dimension = 3
    
    def __init__(self, argdict):
        super(BoxDomain, self).__init__(argdict)
        self.side_length = argdict["side_length"]


    @staticmethod
    def parameters():
        par_dict = super(BoxDomain, BoxDomain).parameters()
        par_dict.update(
            {
                "side_length": ("Side length of box", 30.0)
            }
        )
        return par_dict

    def create_domain(self):
        S = self.side_length
        domain = Box(Point(0, 0, 0), Point(S, S, S))

        return domain



    def domain_description(self, unit="microns"):
        return (
            "A box of side length {s} {u}."
        ).format(s=self.side_length, res=self.mesh_resolution, u=unit)




