from base_domain import BaseHoledBoxDomain
from dolfin import Point
from mshr import (Rectangle, CSGTranslation, Polygon)

class FlatOctagonalHoleDomain(BaseHoledBoxDomain):

    dimension = 2
    
    def __init__(self, argdict):
        super(FlatOctagonalHoleDomain, self).__init__(argdict)
        
        self.tunnel_width = argdict["tunnel_width"]
        self.cell_diam = argdict["cell_diam"]
        self.C = argdict["corner_size"]
        self.S = self.cell_diam - 2*self.C
        if self.S <= 0:
            raise Exception("cell diameter must be > 2 * corner size")

        self.Nx = argdict["Nx"]
        self.Ny = argdict["Ny"]


        
    def domain_description(self, unit="microns"):
        return (
            "A rectangular 2D tube of with a grid of "
            " {Nx} by {Ny} octagonal holes of width {cell_diam} {u} "
            " and corner size {C} {u} separated "
            " by a distance of {T} {u}. Mesh has resolution {res}."
        ).format(Nx=self.Nx, Ny=self.Ny, cell_diam=self.cell_diam,
                 C=self.C, T=self.tunnel_width,
                 res=self.mesh_resolution, u=unit
        )

    
    @staticmethod
    def parameters():
        par_dict = super(FlatOctagonalHoleDomain, FlatOctagonalHoleDomain).parameters()


        par_dict.update(
            {
                "Nx": ("Number of tubular holes in x-direction.", 2),
                "Ny": ("Number of tubular holes in y-direction.", 1),
                "tunnel_width": ("Distance between tubular holes.", 5.0),
                "corner_size": ("Corner size of tubular holes.", 2.0),
                "cell_diam": ("Diameter of tubular holes.", 30.0),
            }
        )
        return par_dict

    
    def create_domain(self):
        S, C, T = self.S, self.C, self.tunnel_width
        Nx, Ny = self.Nx, self.Ny
        octagon_vertex_coords = [
            (C, 0),
            (S+C, 0),
            (S+2*C, C),
            (S+2*C, S+C),
            (S+C, S+2*C),
            (C, S+2*C),
            (0, S+C),
            (0, C)
        ]

        octagon_vertices = [Point(*coords)
                            for coords in octagon_vertex_coords]

        octagon = Polygon(octagon_vertices)


        translate_length = self.cell_diam + T
        holes = [CSGTranslation(octagon, Point(n*translate_length,
                                               m*translate_length))
                 for n in range(Nx)
                 for m in range(Ny)]

        self.xmin = -T/2
        self.xmax = Nx*translate_length - T/2
        self.ymin = -T/2
        self.ymax = Ny*translate_length - T/2

        domain = Rectangle(Point(self.xmin, self.ymin),
                           Point(self.xmax, self.ymax))

        for hole in holes:
            domain = domain - hole

        return domain


