from base_domain import BaseDomain
from dolfin import Point, FacetFunction, SubDomain
from mshr import Circle
import time

class CircleDomain(BaseDomain):

    dimension = 2
    
    def __init__(self, argdict):
        super(CircleDomain, self).__init__(argdict)
        self.radius = argdict["radius"]
        self.num_segments = argdict["num_segments"]


    def domain_description(self, unit="microns"):
        return (
            "A square of side length {s} {u}. Mesh has resolution {res}."
        ).format(s=self.radius, res=self.mesh_resolution, u=unit)
    
    @staticmethod
    def parameters():
        par_dict = super(CircleDomain, CircleDomain).parameters()
        par_dict.update(
            {
                "radius": ("Radius of circle", 30.0),
                "num_segments": ("Number of segments to use for 'circle'.", 32)
            }
        )
        return par_dict


    def create_domain(self):
        domain = Circle(Point(0, 0), self.radius, segments=self.num_segments)
        return domain

    def partition_boundary(self):
        boundary_parts = FacetFunction("size_t", self.mesh)
        
        near_eps = self.near_eps
        
        class CircleBoundary(SubDomain): 
            def inside(self, x, on_boundary):
                return on_boundary
        CircleBoundary().mark(boundary_parts, 1)
            
        return boundary_parts


