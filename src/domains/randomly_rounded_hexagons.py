from base_domain import BaseHoledBoxDomain
from dolfin import Point
from math import pi, sqrt, sin, cos
from mshr import (Box, CSGTranslation, CSGRotation, generate_mesh, Circle,
                  Extrude2D, Polygon, Rectangle)
from numpy import linspace
# from random import betavariate
import random



class RandomlyRoundedHexagonalHoleDomain(BaseHoledBoxDomain):
    dimension = 3
    
    def __init__(self, argdict):
        super(RandomlyRoundedHexagonalHoleDomain, self).__init__(argdict)

        self.length = float(argdict["length"])
        self.sheet_width = float(argdict["sheet_width"])
        self.side_length = float(argdict["side_length"])


        self.r_max = self.side_length * sqrt(3)/2

        self.n_round = float(argdict["n_round"])
        if not( 1 <= self.n_round):
            raise Exception("need at least 1 point for circle approximation")

        self.Nz = argdict["Nz"]
        self.Ny = argdict["Ny"]
        self.resolution = float(argdict["resolution"])

        self.alpha = argdict["alpha"]
        self.beta = argdict["beta"]
        self._rounding_radiuses = {}
        self.deterministic = argdict["deterministic"]
        if argdict["seed"] is not None:
            print "**using seeded random generator!**"
            random.seed(argdict["seed"])

    def description(self, unit="microns"):
        return (
            " A rectangular tube of length {l} with a grid {Ny} rows of {Nz} "
            "hexagons hexagonal holes going parallel to the tube. "
            "(With some extra hexagons to make borders nice.) Holes had "
            " side length {side_length} and corners rounded by an angle of"
            " {theta}, and were separated by a distance of {sheet_length} "
            "microns. Mesh has resolution {res} was used."
        ).format(Nz=self.Nz, Ny=self.Ny,
                 side_length=self.side_length, C=self.C, l=self.length,
                 sheet_length=self.sheet_length, res=self.mesh_resolution
        )


    def create_mesh(self):
        # don't use Tetgen because it absolutely hates my circles
        domain = self.create_domain()
        self._mesh = generate_mesh(domain, int(self.resolution))
        return self._mesh

    def rounding_radiuses(self, n, m):
        """Returns list of rounding radiuses of each corner of the hexagon with
        coords (n, m) in ccw order, starting with the right vertex's tunnel."""

        l = []
        for k in [(3*n+1, 3*m), (3*n-1, 3*m+3), (3*n-2, 3*m+3),
                  (3*n-1, 3*m), (3*n+1, 3*m-3), (3*n+2, 3*m-3)]:
            if k not in self._rounding_radiuses:
                if self.deterministic:
                    self._rounding_radiuses[k] = (self.r_max * self.alpha
                                                  / (self.alpha + self.beta))
                else:
                    self._rounding_radiuses[k] = (self.r_max *
                                                  random.betavariate(self.alpha,
                                                                     self.beta))
            l.append(self._rounding_radiuses[k])

        return l

    def tunnel_volume(self, EPS=1E-5):
        # import IPython; IPython.embed()
        """Returns total tunnel volume of domain. EPS is tolerance."""
        # no longer needed: # Make sure to call only **after** generating mesh.
        
        w = self.sheet_width
        def _tunnel_volume(crds):
            r = self._rounding_radiuses[crds]
            return ((sqrt(3) - pi/2) * r**2
                    + sqrt(3) * r*w
                    + sqrt(3)/4 * w**2) * self.length


        
        def _is_valid(crds):    # exclude hexa vertices entirely outside domain            
            a = crds[0]/3.0     # n-crds in lattice 
            b = crds[1]/3       # m-crds in lattice - integer div. intentional

            if (b < 0 or b > self.Nz):
                return False
            
            if b %2 == 0:       # even rows start and end with a hex center
                return (a >= -(b/2)
                        and a <= self.Ny - ((b+1)/2))
            else:               # odd rows start and end right outside a hex
                return (a >= (-(b/2) - 0.5)
                        and a <= (self.Ny - ((b+1)/2) + 0.5))
        
        tunnel_crds = filter(_is_valid, self._rounding_radiuses)
        
        min_z = 0
        max_z = self.Nz
        edge_tunnel_crds = filter(lambda crds: (crds[1] == min_z
                                            or crds[1] == 3*max_z),
                                  tunnel_crds)
        interior_tunnel_crds = filter(lambda crds: not crds in edge_tunnel_crds,
                                      tunnel_crds) 
        
        edge_volumes = map(lambda crds: _tunnel_volume(crds),
                           edge_tunnel_crds)
        
        interior_volumes = map(lambda crds: _tunnel_volume(crds),
                               interior_tunnel_crds)

        # import IPython; IPython.embed()
        return (sum(interior_volumes) + sum(edge_volumes) / 2)

            
    
    @staticmethod
    def parameters():
        par_dict = super(RandomlyRoundedHexagonalHoleDomain,
                         RandomlyRoundedHexagonalHoleDomain).parameters()
        par_dict.update(
            {
                "Nz": ("Number of tubular holes in z-direction.", 2, int),
                "Ny": ("Number of tubular holes in y-direction.", 1, int),
                "n_round": ("Number of points to approximate circle section with.", 10, int),
                "sheet_width": ("Distance between tubular holes in microns.", 5, float),

                "side_length": ("Side length of hexagons.", 30.0, float),

                "length": ("Length of tube in microns.", 30.0, float),
                "alpha": ("alpha parameter for beta distribution.", 4.0, float),
                "beta": ("beta parameter for beta distribution.", 4.0, float),
                "deterministic": ("No randomness.", False),
                "seed": ("Seed for RNG.", None, int),
            }
        )
        return par_dict

    def _60_degree_rotate(self, pt):
        """Rotates a point 60 degrees about the origin."""
        x, y = pt
        return (x/2 - y * sqrt(3)/2, y/2 + x * sqrt(3)/2)

    def _rotate(self, pt, theta):
        """Rotates a point theta radians about the origin."""
        x, y = pt
        s, c = sin(theta), cos(theta)
        return (c*x - s*y, s*x + c*y)
    
    def _rounded_hexagon(self, a, n, m):
        rounding_radiuses = self.rounding_radiuses(n, m)



        hexagon_vertex_coords = []
        for i in range(6):
            r = rounding_radiuses[i]
            ctr = self._rotate((a - r * 2/sqrt(3), 0), i*pi/3)
            corner = [(ctr[0] + r * cos(angle), 
                       ctr[1] + r * sin(angle))
                      for angle in linspace(i*pi/3 - pi/6,
                                            i*pi/3 + pi/6,
                                            self.n_round)] 
            hexagon_vertex_coords.extend(corner)



        hexagon = Polygon([Point(*coords)
                           for coords in hexagon_vertex_coords])
        return hexagon

        
    def create_domain(self):
        S, w = self.side_length, self.sheet_width
        Nz, Ny = self.Nz, self.Ny


        
        holes = []

        b0 = (3*S + w * sqrt(3), 0)          # n-coordinate
        b1 = (b0[0]/2, (sqrt(3)*S+w)/2)      # m-coordinate
        
        for m in range(self.Nz + 1):
            for n in range(-(m/2), self.Ny + 1 - ((m+1)/2)):
                hexagon = self._rounded_hexagon(S, n, m)
                hexagon = CSGTranslation(hexagon,
                                         Point(n*b0[0] + m*b1[0],
                                               n*b0[1] + m*b1[1]))
                holes.append(hexagon)


        domain = Rectangle(Point(0, 0),
                           Point(Ny*b0[0], Nz*b1[1]))
        for hole in holes:
            domain = domain - hole

        domain = CSGRotation(Extrude2D(domain, self.length),
                             Point(0, 1, 0),
                             Point(0, 0, 0), -pi/2)

        return domain

    
