from base_domain import BaseDomain
from dolfin import Point, FacetFunction, SubDomain, near, FaceFunction, SubDomain
from mshr import Cylinder
import time

class CylinderDomain(BaseDomain):

    dimension = 3
    
    def __init__(self, argdict):
        super(CylinderDomain, self).__init__(argdict)
        self.radius = argdict["radius"]
        self.length = argdict["length"]
        self.resolution = argdict["resolution"]

    def description(self, unit="microns"):
        return (
            "A cylinder tube of length {l} and radius {r} {u}.\n"
            " Mesh has resolution {res} was used."
        ).format(l=self.length, r=self.radius,
                 res=self.mesh_resolution, u=unit
        )

    @staticmethod
    def parameters():
        par_dict = super(CylinderDomain, CylinderDomain).parameters()
        par_dict.update(
            {
                "radius": ("Radius of tube", 8.0),
                "length": ("Length of tube", 30.0)
            }
        )
        return par_dict

    def create_domain(self):

        l = self.length
        r = self.radius

        domain = Cylinder(Point(0, 0, 0), Point(l, 0, 0), r, r)

        return domain

    def partition_boundary(self):
        boundary_parts = FacetFunction("size_t", self.mesh)
        
        xmin, xmax = self.get_coord_minmax(self.mesh)[:2]
        
        class CylinderBoundary(SubDomain): 
            def inside(self, x, on_boundary):
                # hard to specify hole boundaries, so mark everything
                # and then re-mark that which is not a hole
                return on_boundary
        CylinderBoundary().mark(boundary_parts, 1)


        class InflowBoundary(SubDomain): 
            def inside(self, x, on_boundary):
                return (near(x[0], xmin) and on_boundary)

        InflowBoundary().mark(boundary_parts, 2)

        class OutflowBoundary(SubDomain): 
            def inside(self, x, on_boundary):
                return (near(x[0], xmax) and on_boundary)

        OutflowBoundary().mark(boundary_parts, 3)
        
        return boundary_parts


