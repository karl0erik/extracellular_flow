from base_domain import FileDomain, BaseHoledBoxDomain
from dolfin import Mesh, XDMFFile, mpi_comm_world


    
class KinneyDomain(FileDomain, BaseHoledBoxDomain):
    dimension = 3
    
    # def __init__(self, argdict, fn):
    #     super(KinneyDomain, self).__init__(argdict, fn)

