from base_domain import BaseHoledBoxDomain
from dolfin import Point
from math import pi
from mshr import (Box, CSGTranslation, CSGRotation, generate_mesh, Cylinder,
                  Extrude2D, Polygon)

class ParallelCylinderHoleDomain(BaseHoledBoxDomain):
    dimension = 3
    
    def __init__(self, argdict):
        super(ParallelCylinderHoleDomain, self).__init__(argdict)

        self.length = argdict["length"]
        self.cylinder_sep = argdict["cylinder_sep"]
        self.cell_radius = argdict["cell_radius"]

        # if self.S <= 0:
        #     raise Exception("cell diameter must be > 2 * corner size")

        self.Nz = argdict["Nz"]
        self.Ny = argdict["Ny"]
        self.resolution = argdict["resolution"]

    def description(self, unit="microns"):
        return (
            " A rectangular tube of length {l} with a grid of "
            " {Nz} by {Ny} cylinderal holes going parallel to the tube "
            " of width {cell_radius} microns and corner size {C} microns separated "
            "by a distance of {T} microns. Mesh has resolution {res} was used."
        ).format(Nz=self.Nz, Ny=self.Ny,
                 cell_radius=self.cell_radius, C=self.C, l=self.length,
                 T=self.cylinder_sep, res=self.mesh_resolution
        )


        
    @staticmethod
    def parameters():
        par_dict = super(ParallelCylinderHoleDomain, ParallelCylinderHoleDomain).parameters()
        par_dict.update(
            {
                "Nz": ("Number of tubular holes in z-direction.", 2, int),
                "Ny": ("Number of tubular holes in y-direction.", 1, int),
                "cylinder_sep": ("Distance between tubular holes in microns.",
                                        5, float),
                "cell_radius": ("Diameter of tubular holes in microns.", 30, float),
                "length": ("Length of tube in microns.", 30, float)
            }
        )
        return par_dict

    
    def create_domain(self):
        T, l = [self.cylinder_sep, self.length]
        Nz, Ny = self.Nz, self.Ny
        
        # cylinder_tube = Cylinder(Point(0, 0, 0), Point(0, 0, l),
        #                          self.cell_radius, self.cell_radius)
        
        # cylinder_tube = CSGRotation(cylinder_tube, # Extrude2D(cylinder_tube, l),
        #                             Point(0, 1, 0),
        #                             Point(self.cell_radius, 0, self.cell_radius), -pi/2)
        
        cylinder_tube = Cylinder(Point(0, 0, 0), Point(l, 0, 0),
                                 self.cell_radius, self.cell_radius)
        
        translate_length = 2*self.cell_radius + T
        holes = [CSGTranslation(cylinder_tube, Point(0, n*translate_length,
                                                    m*translate_length))
                 for n in range(Ny)
                 for m in range(Nz)]


        eps = 1E-4
        self.xmin = 0 + eps
        self.xmax = l * (1 - eps)
        self.ymin = -(T/2 + self.cell_radius) * (1 + eps)
        self.ymax = (Ny*translate_length + self.ymin) * (1 - eps)
        self.zmin = -(T/2 + self.cell_radius) * (1 + eps)
        self.zmax = (Nz*translate_length + self.zmin) * (1 - eps)
        
        domain = Box(Point(self.xmin, self.ymin, self.zmin),
                     Point(self.xmax, self.ymax, self.zmax))

        for hole in holes:
            domain = domain - hole

        return domain
