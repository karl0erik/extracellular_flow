import sys, os
from dolfin import *

from problems import StokesProblem
from domains import ZebrafishDomain
from utils import default_arg_dict, print_message


"""
Solve a Stokes problem on Zebrafish ECS mesh. 
Call as pyton demo.py domains/zebrafish.xml.gz
""" 

zero = Constant((0, 0, 0))
ppx = 9.375E-8                                # pressure gradient in uPa/um
p_top = ppx * 192
p_mid = ppx * 105
p_bot = ppx * 0
mu = 3.5E-9                                   # dynamic viscosity (uPa * s)

mesh_fn = sys.argv[1]                         # try domains/small_cutout

p_argdict = default_arg_dict(StokesProblem)
d_argdict = default_arg_dict(ZebrafishDomain)

domain = ZebrafishDomain(d_argdict, mesh_fn)

p_argdict.update(
    {
        "solver": "gmres",
        "preconditioner": "hypre_amg",
        "use_P1P1": False,
        "mu": mu
    }
)


initial_guess = Expression(("0", "0", "0", "x[1] * dp"), dp=ppx, degree=1)
p_argdict["initial_guess"]=initial_guess      # give the solver a good start


## BCs
symm_bcs = {}                                 # symmetry bcs, ignore this
dirichlet_bcs = {1: zero}                     # noslip BCs on walls
neumann_bcs = {                               # set pressure on inflows/outflows
    2: Constant(p_bot), 3: Constant(p_bot),
    4: Constant(p_mid),
    5: Constant(p_top), 6: Constant(p_top)
}  


problem = StokesProblem(p_argdict, domain,
                        dirichlet_bcs, neumann_bcs, symm_bcs)

u, p = problem.solve()

f = File("zebra_u.xdmf")
f << u

h = File("zebra_p.xdmf")
h << p
