from dolfin import (FacetNormal, assemble, MPI, mpi_comm_world,
                    dot, Expression, Mesh, XDMFFile)
from math import pi, sqrt
import argparse
import pickle

def noslip(subdomain_id, dim=3):
    """Helper method to make a pair (subdomain_id, Constant((0,0,0)))."""
    return (subdomain_id, Constant(tuple([0])*dim))

def default_arg_dict(cls):
    """Returns argdict with all default arguments for given problem or domain class."""
    return {parname: cls.parameters()[parname][1]
            for parname in cls.parameters()}

def get_arg_parser(problem_cls, domain_cls):
    """Creates argparser for a given problem on a given domain."""

    parser = argparse.ArgumentParser(description="")

    args_to_parse = problem_cls.parameters()
    args_to_pars.update(domain_cls.parameters())

    for arg_name in args_to_parse:
        arg_desc, arg_val = args_to_parse[arg_name]
        
        if type(arg_val) is bool:
            parser.add_argument('--' + arg_name, action="store_true",
                                help=arg_desc)
        else:
            parser.add_argument('-' + argname, type=type(arg_val),
                                default=arg_val, help=arg_desc)

    return parser

def load_mesh(filename):
    if filename[-5:] == ".xdmf":
        mesh = Mesh()
        f = XDMFFile(mpi_comm_world(), filename)
        f.read(mesh, True) #tries to load partition from file
        return mesh
    else:
        return Mesh(filename) 


def is_primary_process():
    return MPI().rank(mpi_comm_world()) == 0

def print_message(message):
    """Helper function for handling output in parallel."""
    if is_primary_process():
        print message
    else:
        pass

def pickle_data(data, fn):
    """Helper function for pickling in parallel."""
    if is_primary_process():
        with open(fn, "w") as f:
            print "Pickling some data"
            pickle.dump(data, f)
    else:
        pass 


    
def center_droplet_initial_condition(domain, total_amount, radius):
    """Expression being constant in a small droplet in the middle of the 
    domain. Assumes that a disk with center at the center of the domain
    and radius the given radius is fully contained in the domain."""

    minmax = domain.get_coord_minmax(domain.mesh)
    center = [(minmax[2*n] + minmax[2*n + 1])/2.0
              for n in range(len(minmax)/2)]
    print center

    concentration = total_amount/float(pi * radius**2)

    
    def in_disk(x):
        # print x, center, len(center)
        return sqrt(sum([(x[n] - center[n])**2
                         for n in range(len(center))])) < radius
    
    class Droplet(Expression):
        def eval(self, v, x):
            if in_disk(x):
                v[0] = concentration
            else:
                v[0] = 0
    
    return Droplet()

    
