import sys

from dolfin import *
from problems import PoissonProblem
from domains import KinneyDomain, TubularOctagonalHoleDomain
from utils import default_arg_dict

"""Debug partitioning of Kinney ECS mesh.""" 


mesh_fn = sys.argv[1]           # will be loaded by dolfin as Mesh(mesh_fn)


arg_dict = default_arg_dict(KinneyDomain)
arg_dict["debug"] = True
domain = KinneyDomain(arg_dict, mesh_fn)


mesh = domain.create_mesh()
boundary_parts = domain.partition_boundary(mesh)
f = File("debug_bdy.pvd")
f << boundary_parts






# argdict = {"atol": 1E-3, "rtol": 1E-3, # kind of high, but whatever
#            "random_guess": False, "suppress_debug": False}

# dirichlet_bcs = {2: Constant(0),
#                  3: Constant(1)}
# neumann_bcs = {}                # only homogeneous Neumann BCs

# problem = PoissonProblem(argdict, domain, dirichlet_bcs, neumann_bcs)
# u = problem.solve()

# from dolfin import plot, interactive
# plot(u)
# interactive()


