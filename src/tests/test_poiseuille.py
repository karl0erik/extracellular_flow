import pytest
from domains import CylinderDomain
from problems import StokesProblem
from utils import default_arg_dict
from numpy.testing import assert_allclose
from dolfin import Constant


@pytest.mark.parametrize(("mu", "L", "r", "pi", "po", "resolution"), [
    (1E3, 40.0, 5.0, 0, 133E3, 5.0),
    (1E3, 40.0, 5.0, 0, 133E3, 10.0)

])
def test_HP_flow(mu, L, r, pi, po, resolution):
    """Checks that simulated flow obeys Hagen-Poiseuille equation."""

    
    argdict = default_arg_dict(CylinderDomain)

    argdict["mu"] = mu
    argdict["length"] = L
    argdict["radius"] = r
    argdict["resolution"] = resolution
    argdict["suppress_debug"] = True
    argdict["random_guess"] = False

    argdict["atol"] = 1E-7
    argdict["rtol"] = 1E-7
    
    domain = CylinderDomain(argdict)

    noslip_subdomains = {1: Constant((0, 0, 0))}
    pressure_subdomains = {2: Constant(pi), 3: Constant(po)}
    problem = StokesProblem(argdict, domain, noslip_subdomains, pressure_subdomains)
    

    u, p = problem.solve()

    avg_vel, _ = problem.avg_velocity(u)

    d = 2*r
    dP = po - pi

    expected_flow = dP * d**2 / (32 * mu * L)
    assert_allclose([avg_vel], [expected_flow],
                    rtol=1E-2, atol=1E-2)
