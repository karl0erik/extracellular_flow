import sys, os
import numpy as np
from dolfin import *
from problems import TimeDependentPoissonProblem
from utils import default_arg_dict, print_message, is_primary_process
from domains import *

"""Solve a time deoendent Poisson problem on Kinney ECS mesh.""" 

output_dir = "output_files"
if is_primary_process():
    try:
        os.makedirs(output_dir)
    except OSError, e:
        pass
    

mesh_fn = sys.argv[1]
Nt = int(sys.argv[2])
dt = float(sys.argv[3])

solver = "cg"
preconditioner = "hypre_amg"
max_its = 500
print_message("Using solver {} and preconditioner {}".format(solver, preconditioner))




p_argdict, d_argdict = map(default_arg_dict, [TimeDependentPoissonProblem,
                                              KinneyDomain])
                                              # SquareDomain])

d_argdict["near_eps"] = 1.0
# d_argdict["side_length"] = 1.0

# domain = SquareDomain(d_argdict)
domain = KinneyDomain(d_argdict, mesh_fn)

p_argdict.update(
    {
        "atol": 5E-7, "rtol": 5E-7,
        "random_guess": False,
        "suppress_debug": True,
        "elt_degree": 1,

        "solver": solver,
        "preconditioner": preconditioner,
        "max_its": max_its,
        "D": Constant(2.46E8),  # number from Xie in nm/s
        "dt": Constant(dt)     # 
    }
)


## BCs
dirichlet_bcs = {
    3: Constant(1),
    # 3: Constant(0)
}



## problem
problem = TimeDependentPoissonProblem(p_argdict, domain,
                                      dirichlet_bcs)
problem.setup()

# plot(domain.subdomain_data)
# interactive()

ds = domain.measure('ds')
endwall_area = assemble(Constant(1) * ds(2))






for i in range(1, Nt+1):
    print_message("--- At timestep {} (t={})".format(i, i*dt))
    problem.next()
    avg_temp = assemble(problem.u_curr * ds(2)) / endwall_area
    print_message("    avg temp: {})".format(avg_temp))
    if avg_temp > 0.5:
        print_message('      finished at timestep {} (t={}) with avg temp of {}'.format(i, i*dt, avg_temp))
        
        
        break

f = XDMFFile(mpi_comm_world(), "{}/poisson_u_final.xdmf".format(output_dir))
f.write(problem.u_curr)
    


