import sys

from dolfin import *
from problems import TimeDependentPoissonProblem
from domains import KinneyDomain, BoxDomain
from utils import default_arg_dict, pickle_data, print_message, is_primary_process

"""Solve a Poisson problem on Kinney ECS mesh.""" 

mesh_fn = sys.argv[1]           # will be loaded by dolfin as Mesh(mesh_fn)

p_arg_dict, d_arg_dict = map(default_arg_dict, [TimeDependentPoissonProblem,
                                                KinneyDomain])
                                                # BoxDomain])
domain = KinneyDomain(d_arg_dict, mesh_fn)
# d_arg_dict["resolution"] = 0.001
# d_arg_dict["side_length"] = 5000
# domain = BoxDomain(d_arg_dict)


## Mesh quality checks
# mq = MeshQuality()
# hist = mq.radius_ratio_histogram_data(domain.mesh)
# pickle_data(hist, "histogram.pickle")


T = 1.5E-2
Nt = 40
dt = T/Nt
p_arg_dict["solver"] = "gmres"
p_arg_dict["preconditioner"] = "hypre_amg"

p_arg_dict["Nt"] = Nt
p_arg_dict["D"] = 5.92E8
p_arg_dict["dt"] = dt

p_arg_dict["atol"] = 1E-8
p_arg_dict["rtol"] = 1E-8
p_arg_dict["suppress_debug"] = False

inflow_temp = 1
dirichlet_bcs = {2: Constant(inflow_temp)}


problem = TimeDependentPoissonProblem(p_arg_dict, domain, dirichlet_bcs)

mov = File("output_files/animation.pvd")
# mov = XDMFFile(mpi_comm_world(), "output_files/animation.pvd")

u = problem.setup()

ds = domain.measure("ds")
outflow_area = assemble(1 * ds(3)) # outflow = opposite edge, sorry about the name

outflow_temp = 0
Nt = 0

while Nt < 1000 and outflow_temp < 0.5:
    Nt += 1
    u = problem.solve()
    outflow_temp = (assemble(u * ds(3))/outflow_area)/inflow_temp
    print_message("-- t= {}: \t out_temp: {} %".format(problem.t,
                                                       outflow_temp))

print_message("Temp reached {} at t={}, after {} steps".format(outflow_temp,
                                                               problem.t,
                                                               Nt))
    
# for u in problem.solve():
#     # if frame_count % 10 == 0:
#     mov << u
#     frame_count += 1

print "Done!"
