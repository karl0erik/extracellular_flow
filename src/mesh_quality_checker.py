import sys, pickle
import numpy as np
from dolfin import *
import pylab as pl
from problems import PoissonProblem, StokesProblem
from domains import *
from utils import *
import matplotlib.pyplot as plt

kinney_mesh_fn = sys.argv[1]

def default(tpl):
    dom_cls, r = tpl
    print dom_cls, r
    arg_dict = default_arg_dict(dom_cls)
    arg_dict["resolution"] = r
    return dom_cls(arg_dict), dom_cls.__name__

# doms = map(default, [
#     (BoxDomain, 300.0),
#     (CylinderDomain, 870.0),
#     (TubularOctagonalHoleDomain, 500.0),
#     (ParallelOctagonalHoleDomain, 570.0)])

doms = [(KinneyDomain(default_arg_dict(KinneyDomain), kinney_mesh_fn), "Kinney2C")]

for dom, name in doms:
    m = dom.mesh                # hack
for dom, name in doms:
    m = dom.mesh
    mq = MeshQuality()
    data = mq.radius_ratios(m)
    pl.gca().set_xscale("log")
    pl.gca().set_yscale("log")
    # pl.show()
    # print np.logspace(-5, 0, 50)
    plt.hist(data, bins=np.logspace(-5, 0, 50))

    # hist1 = mq.radius_ratio_histogram_data(m)
    # hist2 = mq.dihedral_angles_histogram_data(m)
    # print name, sum(hist1[1]), sum(hist1[0])
    # print name, sum(hist2[1]), sum(hist2[0])
    # plt.plot(*hist1)
    plt.title(name + "RR")
    plt.savefig("{}_RR_log.png".format(name))
    plt.close()

    # plt.plot(*hist2)
    # plt.title(name + "DA")
    # plt.savefig("{}_DA.png".format(name))
    # plt.close()
    # plt.show()
