import sys, os
import numpy as np
from dolfin import *
from problems import StokesProblem
from utils import default_arg_dict, print_message, is_primary_process
from domains import KinneyDomain, BoxDomain, ParallelOctagonalHoleDomain

"""Solve a Stokes problem on Kinney ECS mesh.""" 

output_dir = "output_files"
if is_primary_process():
    try:
        os.makedirs(output_dir)
    except OSError, e:
        pass
    


mesh_fn = sys.argv[1]           # will be loaded by dolfin as Mesh(mesh_fn)
solver = sys.argv[2]
preconditioner = sys.argv[3]
print_message("Using solver {} and preconditioner {}".format(solver, preconditioner))
gmres_restarts = int(sys.argv[4])
max_its = int(sys.argv[5])
use_P1P1 = (sys.argv[6] == "P1P1")  # P1P1 for stabil. P1P1, otherwise TH

if len(sys.argv) > 7:
    flow_direction = sys.argv[7]
    print_message("Using flow direction " + flow_direction)
else:
    flow_direction = "x"


p_argdict, d_argdict = map(default_arg_dict, [StokesProblem,
                                              KinneyDomain])
                                              # ParallelOctagonalHoleDomain])
                                              # BoxDomain])

domain = KinneyDomain(d_argdict, mesh_fn)

# d_argdict["resolution"] = 3.0
# domain = ParallelOctagonalHoleDomain(d_argdict)
# domain = BoxDomain(d_argdict)

p_argdict.update(
    {
        "atol": 5E-13, "rtol": 5E-13,
        "random_guess": False,
        "suppress_debug": False,
        "debug_mode": False,
        "mu": 0.8E-3,           
        "solver": solver,
        "preconditioner": preconditioner,
        "gmres_restarts": gmres_restarts,
        "max_its": max_its,
        "beta": 0.2,
        "use_P1P1": use_P1P1
    }
)

## BCs
zero = Constant((0, 0, 0))
# ppx = 133E-3 # pressure differential of 1 mmHg/mm (in Pa/micron)
ppx = 133E-6 # pressure differential of 1 mmHg/mm (in Pa/nanometer)

# dirichlet_bcs = {
#     1: zero
# }

if flow_direction == "x":
    dirichlet_bcs = {
        i: zero
        for i in range(1, 8)
        if not((i == 2) or (i == 3))
    }
    initial_guess = Expression(("0", "0", "0",
                                "x[0] * dp"), dp=ppx, degree=1)
    
elif flow_direction == "y":
    dirichlet_bcs = {
        i: zero
        for i in range(1, 8)
        if not((i == 4) or (i == 5))
    }
    initial_guess = Expression(("0", "0", "0",
                                "x[1] * dp"), dp=ppx, degree=1)
    
else:                           # assume == "z"
    dirichlet_bcs = {
        i: zero
        for i in range(1, 8)
        for i in range(1, 8)
        if not((i == 6) or (i == 7))
    }
    initial_guess = Expression(("0", "0", "0",
                                "x[2] * dp"), dp=ppx, degree=1)

p_bc = initial_guess[3]

p_argdict["initial_guess"]=initial_guess
neumann_bcs = {i: p_bc for i in range(1, 8)} # set presure everywhere, including interior holes
# neumann_bcs = {i: p_bc for i in [2, 3]} # set pressure only on inflow/outflow
symm_bcs = {}
## problem
problem = StokesProblem(p_argdict, domain,
                        dirichlet_bcs, neumann_bcs, symm_bcs)

u, p = problem.solve()
if is_primary_process():
    np.save("{}/beta_{}".format(output_dir, problem.beta), problem.residuals)

f = File("{}/beta_{}_u.pvd".format(output_dir, problem.beta))
f << u

h = File("{}/beta_{}_p.pvd".format(output_dir, problem.beta))
h << p

# g = File("bdy_parts.pvd")
# g << domain.subdomain_data




