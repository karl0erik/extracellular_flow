#!/bin/bash
# Job name:
#SBATCH --job-name=hexagon_comparison_TH
#
# Project:
#SBATCH --account=nn9279k
# Wall clock limit:
#SBATCH --time='48:00:00'
#
#
# Max memory usage per task:
#SBATCH --mem-per-cpu=3900M
#
# Number of tasks (cores):
#SBATCH --ntasks=16 --nodes=1
#SBATCH --hint=compute_bound
#SBATCH --cpus-per-task=1

## Set up job environment
source /cluster/bin/jobsetup

echo $SCRATCH

source ~johannr/fenics-dev-2016.04.06.abel.gnu.conf

# Expand pythonpath with locally installed packages
# export PYTHONPATH=$PYTHONPATH:$HOME/.local/lib/python2.7/site-packages/

# Define what to do when job is finished (or crashes)
cleanup "mkdir -p /work/users/karleh/hexagon_comparison"
cleanup "mkdir -p /work/users/karleh/hexagon_comparison/$SLURM_JOB_ID"
cleanup "cp -r $SCRATCH/src/output_files /work/users/karleh/hexagon_comparison/$SLURM_JOB_ID"

# Copy necessary files to $SCRATCH
cp -r /usit/abel/u1/karleh/extracellular_flow/src $SCRATCH




# Enter $SCRATCH and run job
cd $SCRATCH/src
mv scripts/hexagon_computations.py . # needed because I am kind of a klutz
python hexagon_computations.py use_TH
# mpirun -np 16 python hexagon_computations.py use_TH
