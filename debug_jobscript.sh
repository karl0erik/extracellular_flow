#!/bin/bash
# Job name:
#SBATCH --job-name=ecs_poisson_test
#
# Project:
#SBATCH --account=nn9279k
# Wall clock limit:
#SBATCH --time='0:15:00'
#
#
# Max memory usage per task:
#SBATCH --mem-per-cpu=3800M
#
# Number of tasks (cores):
#SBATCH --ntasks=16
#SBATCH --hint=compute_bound
#SBATCH --cpus-per-task=1





## Set up job environment
source /cluster/bin/jobsetup

echo $SCRATCH

#source ~oyvinev/intro/hashstack/fenics-1.5.0.abel.gnu.conf
# source ~oyvinev/fenics1.6/fenics1.6
# source ~johannr/fenics-dev-2015.12.14.abel.gnu.conf
source ~johannr/fenics-dev-2016.04.06.abel.gnu.conf

# Expand pythonpath with locally installed packages
# export PYTHONPATH=$PYTHONPATH:$HOME/.local/lib/python2.7/site-packages/

# Define what to do when job is finished (or crashes)
cleanup "mkdir -p /work/users/karleh/ecs_flow"
cleanup "cp -r $SCRATCH /work/users/karleh/ecs_flow"

# Copy necessary files to $SCRATCH
cp -r /usit/abel/u1/karleh/extracellular_flow/src $SCRATCH

# Enter $SCRATCH and run job
cd $SCRATCH/src

# mpirun -np 64 python cli.py fig_2C solve
mpirun -np 16 python kinney_debug.py domains/fig_2C_super_HD.xdmf
