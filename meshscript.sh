#!/bin/bash
# Job name:
#SBATCH --job-name=2E_tinycrop
#
# Project:
#SBATCH --account=nn9279k
# Wall clock limit:
#SBATCH --time='2:00:00'
#
# Max memory usage per task:
#SBATCH --mem-per-cpu=220G --partition=hugemem
#
# Number of tasks (cores):
#SBATCH --ntasks=1
#SBATCH --hint=compute_bound
#SBATCH --cpus-per-task=4





## Set up job environment
source /cluster/bin/jobsetup

echo $SCRATCH

#source ~oyvinev/intro/hashstack/fenics-1.5.0.abel.gnu.conf
# source ~oyvinev/fenics1.6/fenics1.6
# source ~johannr/fenics-dev-2015.12.14.abel.gnu.conf
source ~johannr/fenics-dev-2016.04.06.abel.gnu.conf

# Expand pythonpath with locally installed packages
# export PYTHONPATH=$PYTHONPATH:$HOME/.local/lib/python2.7/site-packages/

# Define what to do when job is finished (or crashes)
cleanup "mkdir -p /work/users/karleh/mesh_jobs"
cleanup "cp -r $SCRATCH /work/users/karleh/mesh_jobs"

# Copy necessary files to $SCRATCH
cp -r /usit/abel/u1/karleh/extracellular_flow/src/scripts/reconstructions.py $SCRATCH
cp -r /usit/abel/u1/karleh/data/kinney_reconstructions/figure_2E_reconstruction $SCRATCH

# Enter $SCRATCH and run job
cd $SCRATCH



python reconstructions.py figure_2E_reconstruction 2E_tinycrop.xdmf 50.0
